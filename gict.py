import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gio
from gi.repository import Gtk
from PIL import Image

# Fonction de conversion d'image
def convert_image(input_path, output_path, format):
    try:
        image = Image.open(input_path)
        image.save(output_path, format=format)
        print("Conversion réussie !")
    except Exception as e:
        print(f"Erreur lors de la conversion : {str(e)}")

# Classe principale de l'application
class ImageConverter(Gtk.Application):
    def __init__(self):
        super().__init__(application_id="com.example.image_converter", flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self):
        # Créer une fenêtre principale
        window = Gtk.ApplicationWindow(application=self)
        window.set_default_size(400, 200)
        window.set_title("Image Converter")

        # Créer les widgets
        input_label = Gtk.Label(label="Chemin de l'image d'entrée:")
        self.input_entry = Gtk.Entry()
        output_label = Gtk.Label(label="Chemin de l'image de sortie:")
        self.output_entry = Gtk.Entry()
        format_label = Gtk.Label(label="Format de sortie:")
        self.format_combo = Gtk.ComboBoxText()
        self.format_combo.append_text("webp")
        self.format_combo.append_text("jpeg")
        self.format_combo.append_text("png")
        self.format_combo.append_text("heif")
        self.format_combo.append_text("heic")
        self.format_combo.append_text("ico")
        convert_button = Gtk.Button(label="Convertir")
        convert_button.connect("clicked", self.on_convert_button_clicked)

        # Créer la mise en page
        grid = Gtk.Grid()
        grid.set_row_spacing(10)
        grid.set_column_spacing(10)
        grid.attach(input_label, 0, 0, 1, 1)
        grid.attach(self.input_entry, 1, 0, 1, 1)
        grid.attach(output_label, 0, 1, 1, 1)
        grid.attach(self.output_entry, 1, 1, 1, 1)
        grid.attach(format_label, 0, 2, 1, 1)
        grid.attach(self.format_combo, 1, 2, 1, 1)
        grid.attach(convert_button, 0, 3, 2, 1)
        window.set_child(grid)

        window.show()

    # Gestionnaire d'événement pour le bouton de conversion
    def on_convert_button_clicked(self, button):
        input_path = self.input_entry.get_text()
        output_path = self.output_entry.get_text()
        format = self.format_combo.get_active_text()
        convert_image(input_path, output_path, format)

# Créer une instance de l'application et la lancer
app = ImageConverter()
app.run(None)
